# Copyright (c) 2014 Ricardo Gomez

# inherit from common qcom
-include device/samsung/qcom-common/BoardConfigCommon.mk

# inherit from the proprietary version
-include vendor/samsung/centura/BoardConfigVendor.mk

# CPU
TARGET_CPU_VARIANT := cortex-a15

# Kernel
BOARD_KERNEL_CMDLINE := androidboot.hardware=qcom loglevel=1
BOARD_KERNEL_BASE := 00200000
BOARD_PAGE_SIZE := 4096

# Assert
# TARGET_OTA_ASSERT_DEVICE := d2tmo
