# Copyright (C) 2014 Ricardo Gomez

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/aosp_base.mk)
# This is where we'd set a backup provider if we had one
#$(call inherit-product, device/sample/products/backup_overlay.mk)
# Inherit from centura device
$(call inherit-product, device/samsung/centura/device.mk)

# Set those variables here to overwrite the inherited values.
PRODUCT_NAME := full_centura
PRODUCT_DEVICE := centura
PRODUCT_BRAND := Android
PRODUCT_MODEL := AOSP on Centura
